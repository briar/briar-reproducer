#!/usr/bin/env bash
set -e
set -x

apt-get install -y --no-install-recommends \
	diffoscope \
	apksigner \
	apktool
