# Briar Reproducer

This is a tool you can use to verify
that [Briar](https://briar.app) was built exactly from the public source code
and no modifications (such as backdoors) were made.

It works by building the app deterministically in a controlled standard environment
and comparing the result to the [official APK file](https://briarproject.org/download.html).

More information about these so called reproducible builds is available at
[reproducible-builds.org](https://reproducible-builds.org/).

The source code for this tool is available at
https://code.briarproject.org/briar/briar-reproducer

## How it works

We are using the Docker container technology to create a controlled and stable environment.
Our image is based on the official Debian stable image
and installs a minimum of software required to build Briar
and to compare the result with the official APK files.

You can find all software that gets installed in the `install*.sh` files.

Then there are two Python scripts.
`reproduce.py` is downloading the official APK
and the source code from the official git repository.
It builds a new APK from the source code
and then hands over to `verify-apk.py`.

This script verifies that the official APK
and the newly-built one are identical bit by bit.
It does so by repacking both APKs in a deterministic way
stripping both the Android v1
and [v2 signature](https://source.android.com/security/apksigning/v2)
from the APK so that only the contents can be are compared.

Both repacked APKs are then hashed with SHA512.
If their hashes are the same, the contents are identical
and we know that the official APK was built from the exact same source.
If the hashes differ,
[diffoscope](https://diffoscope.org/) is used to show where both versions differ.

## How to use

Verify that you have `docker` installed:

    docker --version

If this command does not work,
please [install Docker](https://docs.docker.com/install/)
and continue once it is installed.

### Using our pre-built image

If you trust that our pre-built Docker image was build exactly from *its* source,
you can use it for faster verification.
If not, you can read the next section to learn how to build the image yourself.
Then you are only trusting the official `debian:stable` which is out of our control.

Otherwise, you can skip the next section and move directly to *Run the verification*.

### Building your own image

Check out the source repository:

    git clone https://code.briarproject.org/briar/briar-reproducer.git

Build our Docker image:

    docker build -t briar/reproducer briar-reproducer

### Run the verification

Currently, the verification needs `disorderfs` as a deterministic file-system.
Therefore, please make sure that `fuse` is installed on your host system.

    apt install fuse

To verify a specific version of Briar, run

    docker run --cap-add SYS_ADMIN --device /dev/fuse briar/reproducer:latest ./reproduce.py [tag]

Where `[tag]` is the git tag (source code snapshot) that identifies the version
you want to test, for example `release-1.0.1`.

You can find a list of tags in Briar's
[source code repository](https://code.briarproject.org/akwizgran/briar/tags).

The `SYS_ADMIN` capability and the `fuse` device are required,
so the container can build the app inside a `disorderfs` which shuffles the filesystem.

If the build fails with `fuse: mount failed: Permission denied`, you may need to add the argument `--security-opt apparmor:unconfined` to the Docker command.

## Historical Changes

* Before version `1.4.2`, Briar was build using Java 8. Newer release are built with Java 11
  which produces different byte code breaking reproducibility
  An [old version of briar-reproducer](https://code.briarproject.org/briar/briar-reproducer/tags/pre-1.4.2)
  can be used to verify these releases.
* Before version `1.1.7`, Briar needed a deterministic files system
  to work around an [Android build system bug](https://issuetracker.google.com/issues/110237303)
  which broke reproducibility.
  An [old version of briar-reproducer](https://code.briarproject.org/briar/briar-reproducer/tags/pre-1.1.7)
  can be used to verify these releases.
* Before version `1.1.0`, the path to the APK (`gradle_apk_path` in `apps.json`) was
  `briar-android/build/outputs/apk/release/briar-android-release-unsigned.apk`
