#!/usr/bin/env python3

import hashlib
import json
import sys


def get_app_info(app_name):
    with open('apps.json', 'r') as f:
        return json.load(f)[app_name]


def calc_hash(filename, block_size=65536):
    sha512 = hashlib.sha512()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha512.update(block)
    return sha512.hexdigest()


def fail(msg=""):
    sys.stderr.write(msg + "\n")
    sys.exit(1)
