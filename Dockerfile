FROM debian:bullseye-slim

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
ENV ANDROID_HOME=/opt/android-sdk

WORKDIR /opt/briar-reproducer

# add required files to WORKDIR
ADD install*.sh ./
ADD *.py ./
ADD apps.json ./

RUN ./install.sh

CMD ./reproduce.py
