#!/usr/bin/env python3

import os
import re
import subprocess
import sys
import tempfile
import zipfile
from utils import calc_hash, fail

APK_SIG_FILE = re.compile(r'META-INF/[0-9A-Za-z_\-]+\.(SF|RSA|DSA|EC|MF)')


def main():
    if len(sys.argv) != 3:
        fail("Usage: %s referenceAPK builtAPK" % sys.argv[0])
    if not verify_apk(sys.argv[1], sys.argv[2]): sys.exit(1)


def verify_apk(reference_apk, built_apk):
    if not os.path.isfile(reference_apk):
        sys.stderr.write("Cannot verify: Reference APK '%s' does not exist" % reference_apk)
        return False

    if not os.path.isfile(built_apk):
        sys.stderr.write("Cannot verify: Built APK '%s' does not exist" % built_apk)
        return False

    with tempfile.TemporaryDirectory() as tmp_dir:
        # repack reference APK
        reference_tmp = os.path.join(tmp_dir, os.path.basename(reference_apk))
        repack_apk(reference_apk, reference_tmp)

        # repack built APK
        built_tmp = os.path.join(tmp_dir, os.path.basename(built_apk))
        repack_apk(built_apk, built_tmp)

        # compare hashes of repacked APKs
        if calc_hash(reference_tmp) != calc_hash(built_tmp):
            sys.stderr.write("Mismatch detected. Trying to find reason...\n")
            subprocess.call(['diffoscope', reference_tmp, built_tmp])
            sys.stderr.write("Files do NOT match! :(")
            return False
        else:
            print("Files match! :)")
            return True


def repack_apk(zip_file, new_zip_file):
    # open original zip file
    with zipfile.ZipFile(zip_file, 'r') as f:
        # write to new zip file
        with zipfile.ZipFile(new_zip_file, 'w') as tmp:
            # go through all files
            for info in f.infolist():
                if APK_SIG_FILE.match(info.filename):
                    continue  # ignore signatures
                info.extra = b''  # reset file metadata
                # reset zip versions that keep changing
                info.create_version = 0
                info.extract_version = 0
                # add file to new zip
                tmp.writestr(info, f.read(info.filename))


if __name__ == "__main__":
    main()
