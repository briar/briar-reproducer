#!/usr/bin/env python3

import atexit
import os
import sys
from subprocess import call, check_call, check_output
from verify_apk import verify_apk
from utils import get_app_info, fail

BUILD_DIR = "build"


def main():
    if len(sys.argv) > 3:
        fail("Usage: %s briar [tag]" % sys.argv[0])
    app_name = sys.argv[1]
    tag = sys.argv[2] if len(sys.argv) > 2 else None

    # get app info
    app = get_app_info(app_name)

    # clone/reset repo and checkout tag
    tag = prepare_repo(app["repo_url"], app["repo_default_branch"], app_name, tag)
    version = tag.split('-')[1]

    # download reference binaries (before building to detect missing file early on)
    apk_url = app["reference_url"] % version
    reference_apk = app["reference_apk"] % version
    check_call(['wget', '--no-verbose', apk_url, '-O', reference_apk])

    # use non-deterministic file system for building the app to detect issues
    if not os.path.exists(BUILD_DIR):
        os.makedirs(BUILD_DIR)
    check_call(['disorderfs', '--shuffle-dirents=yes', app_name, BUILD_DIR])
    atexit.register(unmount)

    # build the app
    check_call(["./gradlew", "--no-daemon", app["gradle_task"]], cwd=BUILD_DIR)

    # check if both APKs match
    apk = os.path.join(BUILD_DIR, app["gradle_apk_path"])
    if not verify_apk(reference_apk, apk):
        sys.exit(1)

    print("Version '%s' was built reproducible! :)" % tag)


def prepare_repo(repo_url, repo_branch, repo_dir, tag):
    if os.path.isdir(repo_dir):
        # get latest commits and tags from remote
        check_call(['git', 'fetch', 'origin'], cwd=repo_dir)
        # checkout to latest main HEAD
        check_call(['git', 'checkout', '-f', repo_branch], cwd=repo_dir)
    else:
        # clone repo
        check_call(['git', 'clone', repo_url, repo_dir])

    # undo all changes
    check_call(['git', 'reset', '--hard'], cwd=repo_dir)

    # clean all untracked files and directories (-d) from repo
    check_call(['git', 'clean', '-dffx'], cwd=repo_dir)

    # use latest tag if none given
    if tag is None:
        result = check_output(['git', 'describe', '--abbrev=0', '--tags'], cwd=repo_dir)
        tag = result.decode().rstrip()  # strip away line-break

    # checkout tag
    check_call(['git', 'checkout', tag], cwd=repo_dir)

    # return the tag that was used for the checkout
    return tag


def unmount():
    call(['fusermount', '-u', BUILD_DIR])


if __name__ == "__main__":
    main()
