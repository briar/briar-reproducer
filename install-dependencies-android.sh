#!/usr/bin/env bash
set -e
set -x

# Install Android SDK Manager
wget --no-verbose -O cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip
unzip cmdline-tools.zip
rm cmdline-tools.zip
mkdir -p ${ANDROID_HOME}/cmdline-tools
mv cmdline-tools ${ANDROID_HOME}/cmdline-tools/latest

# Accept all those nasty EULAs
mkdir -p ${ANDROID_HOME}/licenses/
printf "\n8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e\n24333f8a63b6825ea9c5514f83c2829b004d1fee" > ${ANDROID_HOME}/licenses/android-sdk-license
printf "\n84831b9409646a918e30573bab4c9c91346d8abd" > ${ANDROID_HOME}/licenses/android-sdk-preview-license
printf "\n79120722343a6f314e0719f863036c702b0e6b2a\n84831b9409646a918e30573bab4c9c91346d8abd" > ${ANDROID_HOME}/licenses/android-sdk-preview-license-old

# Install platform-tools and ndk-bundle (for stripping libraries)
mkdir /root/.android
touch /root/.android/repositories.cfg
echo y | $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager "platform-tools" "ndk-bundle"
